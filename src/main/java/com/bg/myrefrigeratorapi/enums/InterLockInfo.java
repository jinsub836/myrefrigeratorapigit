package com.bg.myrefrigeratorapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InterLockInfo {
    KAKAO("카카오")
    ,NAVER("네이버")
    ,GOOGLE("구글");

    private final String name;
}
