package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.FoodIngredient;
import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientRequest;
import com.bg.myrefrigeratorapi.repository.FoodIngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FoodIngredientService {
    private final FoodIngredientRepository foodIngredientRepository;



    public void setFoodIngredient(FoodName foodName, FoodIngredientRequest request) {
        FoodIngredient addData = new FoodIngredient();
        addData.setFoodName(foodName);
        addData.setCategory(request.getCategory());
        addData.setFoodIngredient(request.getFoodIngredient());
        addData.setCapacity(request.getCapacity());

        foodIngredientRepository.save(addData);
    }
}
