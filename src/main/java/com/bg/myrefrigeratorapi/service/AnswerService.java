package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Answer;
import com.bg.myrefrigeratorapi.entity.Questions;
import com.bg.myrefrigeratorapi.model.answer.AnswerRequest;
import com.bg.myrefrigeratorapi.repository.AnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class AnswerService {
    private final AnswerRepository answerRepository;

   public void setAnswer(Questions questions , AnswerRequest request){
       Answer addData = new Answer();
       addData.setQuestionsId(questions);
       addData.setAnswerTitle(request.getAnswerTitle());
       addData.setAnswer(request.getAnswer());
       addData.setDateAnswer(LocalDate.now());
       addData.setIsAnswerDelete(request.getIsAnswerDelete());
       addData.setDateAnswerDelete(request.getDateAnswerDelete());

       answerRepository.save(addData);
   }
}


