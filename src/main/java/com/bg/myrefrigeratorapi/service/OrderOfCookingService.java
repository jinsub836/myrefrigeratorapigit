package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.OrderOfCooking;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingRequest;
import com.bg.myrefrigeratorapi.repository.OrderOfCookingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderOfCookingService {
    private final OrderOfCookingRepository orderOfCookingRepository;

    public void setOrderOfCooking(FoodName foodName, OrderOfCookingRequest request) {
        OrderOfCooking addData = new OrderOfCooking();
        addData.setFoodNameId(foodName);
        addData.setRecipe(request.getRecipe());

        orderOfCookingRepository.save(addData);
    }
}
