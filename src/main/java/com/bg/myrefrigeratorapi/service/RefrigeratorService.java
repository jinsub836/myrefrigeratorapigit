package com.bg.myrefrigeratorapi.service;


import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Refrigerator;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorRequest;
import com.bg.myrefrigeratorapi.repository.RefrigeratorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RefrigeratorService {
    private final RefrigeratorRepository refrigeratorRepository;

    public Refrigerator getData(long id) {return refrigeratorRepository.findById(id).orElseThrow();}


    public void setRefrigerator(Member member, RefrigeratorRequest request){
        Refrigerator addData = new Refrigerator();
        addData.setMemberId(member);
        addData.setRefrigeratorFood(request.getRefrigeratorFood());
        addData.setDateRefrigerator(LocalDate.now());

        refrigeratorRepository.save(addData);
    }
}
