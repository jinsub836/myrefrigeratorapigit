package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.RefrigeratorFood;
import com.bg.myrefrigeratorapi.model.refrigeratorfood.RefrigeratorFoodRequest;
import com.bg.myrefrigeratorapi.repository.RefrigeratorFoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RefrigeratorFoodService {
    private final RefrigeratorFoodRepository refrigeratorFoodRepository;

    public void setRefrigeratorFood(RefrigeratorFoodRequest request){
        RefrigeratorFood addData = new RefrigeratorFood();
        addData.setRefrigeratorFood(request.getRefrigeratorFood());
        addData.setDateRefrigeratorFood(LocalDate.now());

        refrigeratorFoodRepository.save(addData);}
}
