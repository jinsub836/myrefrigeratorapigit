package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.model.member.MemberIdDupCheckResponse;
import com.bg.myrefrigeratorapi.model.member.MemberRequest;
import com.bg.myrefrigeratorapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData (long id){return memberRepository.findById(id).orElseThrow();}

    public MemberIdDupCheckResponse getDupCheck (String memberId){
        MemberIdDupCheckResponse response = new MemberIdDupCheckResponse();
        response.setIsNew(isMemberIdDupCheck(memberId));
        return response;
    }


    public void setMember(MemberRequest request) throws Exception{
        if (!isMemberIdDupCheck(request.getMemberId())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member addData = new Member();

        addData.setMemberName(request.getMemberName());
        addData.setMemberId(request.getMemberId());
        addData.setInterlockInfo(request.getInterlockInfo());
        addData.setPassword(request.getPassword());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMail(request.getMail());
        addData.setDateJoin(LocalDateTime.now());
        addData.setIsOut(request.getIsOut());
        addData.setDateOut(request.getDateOut());

        memberRepository.save(addData);
    }
    private boolean isMemberIdDupCheck(String memberId){
        long dupCheck = memberRepository.countByMemberId(memberId);
        return dupCheck <= 0;
    }
}

