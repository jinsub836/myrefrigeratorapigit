package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.entity.Refrigerator;
import com.bg.myrefrigeratorapi.model.FoodNameRequest;
import com.bg.myrefrigeratorapi.repository.FoodNameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class FoodNameService {
    private final FoodNameRepository foodNameRepository;

    public FoodName getData(long id) {
        return foodNameRepository.findById(id).orElseThrow();
    }

    public void SetFoodName(Refrigerator refrigerator, FoodNameRequest request) {
        FoodName addData = new FoodName();
        addData.setRefrigerator(refrigerator);
        addData.setFoodName(request.getFoodName());
        addData.setRecipeImage(request.getRecipeImage());
        addData.setCalorie(request.getCalorie());
        addData.setDateRegister(LocalDate.now());
        addData.setIsRecipeDelete(request.getIsRecipeDelete());
        addData.setDateRecipeDelete(request.getDateRecipeDelete());

        foodNameRepository.save(addData);
    }
}
