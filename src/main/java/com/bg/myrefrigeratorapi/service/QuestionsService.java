package com.bg.myrefrigeratorapi.service;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.entity.Questions;
import com.bg.myrefrigeratorapi.model.question.QuestionsRequest;
import com.bg.myrefrigeratorapi.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class QuestionsService {
    private final QuestionRepository questionRepository;

    public Questions getDataIs(long id) {return questionRepository.findById(id).orElseThrow();}

    public void setQuestion(Member member, QuestionsRequest request){
        Questions addData = new Questions();

        addData.setMemberEntityId(member);
        addData.setQuestionCategory(request.getQuestionCategory());
        addData.setTitle(request.getTitle());
        addData.setQuestionContents(request.getQuestionContents());
        addData.setAnswerStatus(request.getAnswerStatus());
        addData.setDateWrite(LocalDate.now());
        addData.setIsDelete(request.getIsDelete());
        addData.setDateDelete(request.getDateDelete());

        questionRepository.save(addData);}}
