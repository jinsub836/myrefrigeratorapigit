package com.bg.myrefrigeratorapi.entity;

import com.bg.myrefrigeratorapi.enums.FoodIngredientEnums;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class FoodIngredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "foodNameId", nullable = false)
    private FoodName foodName;

    @Column(nullable = false)
    private FoodIngredientEnums category;

    @Column(nullable = false, length = 15)
    private String foodIngredient;

    @Column(nullable = false, length = 10)
    private String capacity;
}
