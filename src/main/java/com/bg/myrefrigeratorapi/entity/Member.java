package com.bg.myrefrigeratorapi.entity;

import com.bg.myrefrigeratorapi.enums.InterLockInfo;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false , length = 20)
    private String memberName;

    @Column(nullable = false , length = 20)
    private String memberId;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false , length = 20)
    private InterLockInfo interlockInfo;

    @Column(nullable = false , length = 20)
    private String password;

    @Column(nullable = false , length = 13)
    private String phoneNumber;

    @Column(nullable = false , length = 30)
    private String mail;

    @Column(nullable = false)
    private LocalDateTime dateJoin;

    @Column(nullable = false)
    private Boolean isOut;

    @Column
    private LocalDate dateOut;}
