package com.bg.myrefrigeratorapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class FoodName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RefrigeratorId")
    private Refrigerator refrigerator;

    @Column(nullable = false, length = 20, unique = true)
    private String foodName;

    @Column(columnDefinition = "TEXT", unique = true)
    private String recipeImage;

    @Column(nullable = false, length = 10)
    private String calorie;

    @Column(nullable = false)
    private LocalDate dateRegister;

    @Column(nullable = false)
    private Boolean isRecipeDelete;

    private LocalDate dateRecipeDelete;
}
