package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Refrigerator;
import com.bg.myrefrigeratorapi.model.FoodNameRequest;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import com.bg.myrefrigeratorapi.service.RefrigeratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/food-name")
public class FoodNameController {
    private final FoodNameService foodNameService;
    private final RefrigeratorService refrigeratorService;

    @PostMapping("/new/refrigerator-id/{refrigeratorId}")
    public String setFoodName(@PathVariable long refrigeratorId, @RequestBody FoodNameRequest request) {
        Refrigerator refrigerator = refrigeratorService.getData(refrigeratorId);
        foodNameService.SetFoodName(refrigerator, request);

        return "OK";
    }
}
