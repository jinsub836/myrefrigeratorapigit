package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorRequest;
import com.bg.myrefrigeratorapi.service.MemberService;
import com.bg.myrefrigeratorapi.service.RefrigeratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v-1/refrigerator")
public class RefrigeratorController {
    private final MemberService memberService;
    private final RefrigeratorService refrigeratorService;

    @PostMapping("/new/member-id/{memberId}")
    public String setRefrigerator(@PathVariable long memberId, @RequestBody RefrigeratorRequest request) {
        Member member = memberService.getData(memberId);
        refrigeratorService.setRefrigerator(member, request);

        return "OK";
    }
}
