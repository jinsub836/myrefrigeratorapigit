package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.Member;
import com.bg.myrefrigeratorapi.model.question.QuestionsRequest;
import com.bg.myrefrigeratorapi.service.MemberService;
import com.bg.myrefrigeratorapi.service.QuestionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/questions")
@RequiredArgsConstructor
public class QuestionsController {
    private final QuestionsService questionsService;
    private final MemberService memberService;

    @PostMapping("/new/{memberEntityId}")
    public String setQuestion(@PathVariable long memberEntityId, @RequestBody QuestionsRequest request){
        Member member = memberService.getData(memberEntityId);
        questionsService.setQuestion(member, request);
        return "등록 완료";
    }
}
