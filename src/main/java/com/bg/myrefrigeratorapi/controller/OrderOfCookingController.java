package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.orderofcooking.OrderOfCookingRequest;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import com.bg.myrefrigeratorapi.service.OrderOfCookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/order-of-cooking")
public class OrderOfCookingController {
    private final OrderOfCookingService orderOfCookingService;
    private final FoodNameService foodNameService;

    @PostMapping("/new/order-of-cooking-id/{orderOfCookingId}")
    public String setOrderOfCooking(@PathVariable long orderOfCookingId, @RequestBody OrderOfCookingRequest request) {
        FoodName foodName = foodNameService.getData(orderOfCookingId);
        orderOfCookingService.setOrderOfCooking(foodName, request);

        return "OK";
    }
}
