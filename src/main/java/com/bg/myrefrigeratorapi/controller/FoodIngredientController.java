package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.entity.FoodName;
import com.bg.myrefrigeratorapi.model.foodIngredient.FoodIngredientRequest;
import com.bg.myrefrigeratorapi.service.FoodIngredientService;
import com.bg.myrefrigeratorapi.service.FoodNameService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/food-ingredient")
public class FoodIngredientController {
    private final FoodIngredientService foodIngredientService;
    private final FoodNameService foodNameService;

    @PostMapping("new/food-name-id/{foodId}")
    public String setFoodIngredient(@PathVariable long foodId, @RequestBody FoodIngredientRequest request) {
        FoodName foodName = foodNameService.getData(foodId);
        foodIngredientService.setFoodIngredient(foodName, request);

        return "OK";

    }
}
