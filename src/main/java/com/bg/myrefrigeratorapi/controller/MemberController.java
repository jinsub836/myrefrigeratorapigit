package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.model.member.MemberIdDupCheckResponse;
import com.bg.myrefrigeratorapi.model.member.MemberRequest;
import com.bg.myrefrigeratorapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/member")
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;


    @GetMapping("/memberId/check")
    public MemberIdDupCheckResponse getDupCheck(
            @RequestParam(name = "memberId") String memberId
    ) {
        return memberService.getDupCheck(memberId);
    }

    @PostMapping("/join")
    public String setMember(@RequestBody MemberRequest request) throws Exception {
        memberService.setMember(request);
        return "회원 가입이 완료 되었습니다.";
    }
}
