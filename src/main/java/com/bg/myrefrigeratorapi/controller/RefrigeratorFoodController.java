package com.bg.myrefrigeratorapi.controller;

import com.bg.myrefrigeratorapi.model.refrigerator.RefrigeratorRequest;
import com.bg.myrefrigeratorapi.model.refrigeratorfood.RefrigeratorFoodRequest;
import com.bg.myrefrigeratorapi.service.RefrigeratorFoodService;
import com.bg.myrefrigeratorapi.service.RefrigeratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v-1/refrigerator_food")
public class RefrigeratorFoodController {
    private final RefrigeratorFoodService refrigeratorFoodService;

    @PostMapping("/new")
    public String setRefrigeratorFood(RefrigeratorFoodRequest request){
        refrigeratorFoodService.setRefrigeratorFood(request);
        return "음식 등록이 완료 되었습니다.";
    }
}
