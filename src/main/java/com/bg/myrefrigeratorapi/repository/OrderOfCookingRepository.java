package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.OrderOfCooking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderOfCookingRepository extends JpaRepository<OrderOfCooking, Long> {
}
