package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.FoodName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodNameRepository extends JpaRepository<FoodName, Long> {
}
