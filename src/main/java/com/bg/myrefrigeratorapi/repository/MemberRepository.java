package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {

    long countByMemberId(String memberId);

}
