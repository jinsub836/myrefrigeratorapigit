package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.FoodIngredient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodIngredientRepository extends JpaRepository<FoodIngredient, Long> {
}
