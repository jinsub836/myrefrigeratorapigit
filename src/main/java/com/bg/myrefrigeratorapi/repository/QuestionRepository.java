package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Questions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Questions, Long> {
}
