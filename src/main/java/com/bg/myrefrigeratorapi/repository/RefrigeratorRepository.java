package com.bg.myrefrigeratorapi.repository;

import com.bg.myrefrigeratorapi.entity.Refrigerator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefrigeratorRepository extends JpaRepository <Refrigerator, Long > {
}
