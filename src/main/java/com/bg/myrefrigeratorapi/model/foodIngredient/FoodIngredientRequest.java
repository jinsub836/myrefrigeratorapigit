package com.bg.myrefrigeratorapi.model.foodIngredient;

import com.bg.myrefrigeratorapi.enums.FoodIngredientEnums;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoodIngredientRequest {
    private FoodIngredientEnums category;
    private String foodIngredient;
    private String capacity;
}
