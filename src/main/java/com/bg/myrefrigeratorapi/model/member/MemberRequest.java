package com.bg.myrefrigeratorapi.model.member;

import com.bg.myrefrigeratorapi.enums.InterLockInfo;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    private String memberName;

    private String memberId;

    private InterLockInfo interlockInfo;

    private String password;

    private String passwordRe;

    private String phoneNumber;

    private String mail;

    private Boolean isOut;

    private LocalDate dateOut;}
