package com.bg.myrefrigeratorapi.model.member;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class MemberItem {

    private Long Id;

    private String memberName;

    private String memberId;

    private LocalDate dateJoin;

}
